import logo from './logo.svg';
import './App.scss';
import React, { useState, useEffect } from 'react';
import axios from 'axios';

import Title from './components/Title';
import Prizes from './components/Prizes';

const url = 'https://lotto.api.rayriffy.com/latest';

function App() {
  const [data, setData] = useState();
  console.log("🚀 ~ file: App.js ~ line 11 ~ App ~ data", data)

  useEffect(() => {

    fetch(url)
      .then(response => response.json())
      .then(result => setData(result.response))
      .catch(error => console.log('error', error));

  }, []);

  return (
    <div className="App">
      { data?.date && <Title date={data?.date} /> }
      { data?.prizes && <Prizes prizes={data?.prizes} /> }
    </div>
  );
}

export default App;

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      maxWidth:'80%',
      margin:'auto',
      alignItems:'center',
      justifyContent:'center',
      '& > *': {
        margin: theme.spacing(1),
        width: theme.spacing(16),
        height: theme.spacing(16),
      },
    },
    paper: {
        display:'flex',
        alignItems:'center',
        justifyContent:'center',
        boxShadow: 'rgba(100, 100, 111, 0.2) 0px 7px 29px 0px',
        fontWeight:'bold'
    }
  }));

function Prizes({ prizes }) {
    const classes = useStyles();
    return (
        <main>
            {
                prizes.map((prize) => (
                    <section key={prize.id} id={prize.id}>
                        <h2>{prize.name} รางวัล {prize.reward}</h2>
                        <p>จำนวน {prize.amount} รางวัล</p>
                        <div className={classes.root}>
                        {prize.number.map((num) => (
                            <Paper elevation={0} className={classes.paper}>{num}</Paper>
                        ))}
                            
                            
                        </div>
                    </section>

                ))
            }
        </main>
    )
}

export default Prizes

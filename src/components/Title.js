import React from 'react'

function Title({date}) {
    return (
        <header>
            <h1>ผลสลากกินแบ่งรัฐบาล {date}</h1>
        </header>
    )
}

export default Title
